FROM ubuntu:22.04 as build_stage
WORKDIR /findent
COPY . .

RUN sed -e 's/archive.ubuntu.com/ftp.gwdg.de\/pub\/linux\/debian/' -i /etc/apt/sources.list && \
    apt-get update && apt-get -y install bison flex make gcc g++
RUN make -C ./bin


FROM ubuntu:22.04
ENV PATH="/findent:${PATH}"
WORKDIR /findent
COPY --from=build_stage /findent/bin/findent ./
COPY --from=build_stage /findent/scripts/ ./
RUN sed -e 's/archive.ubuntu.com/ftp.gwdg.de\/pub\/linux\/debian/' -i /etc/apt/sources.list && \
    apt-get update && apt-get -y install git git-lfs



