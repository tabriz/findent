# findent

This is a lean fork of the findent (indenter for fortran codes) located at https://sourceforge.net/projects/findent/

The original project offers additional features e.g. GUI frontend, builtin lexer and parser, and windows binaries.

## about

findent indents/beautifies/converts Fortran sources. findent will remove trailing spaces and tabs, and convert tabs at the start of a line into spaces.

## features

 - findent supports Fortran-66 up to Fortran-2018
 - findent can convert from fixed form to free form and vice-versa
 - findent honours cpp and coco preprocess statements
 - findent is validated against all constructs in 'Modern Fortran explained, Incorporating Fortran 2018, Metcalf e.a.'
 - findent honours OpenMP conditionals
 - high speed: 50K - 100K lines per second
 - vim, gedit, emacs: findent optionally emits configuration files for these editors to use findent as a plugin.

## installation

prerequisites:
 - make
 - C++ compiler
 - lexical analyzer
 - language parser

### compilation

```bash
cd findent/bin
```

Edit makefile or set the following environment variables (if necessary):
- **$CPP**: C++ compiler, default: `g++`
- **$LEX**: lexical analyzer, default: `flex`
- **$YACC**: language parser, default: `bison`

```bash
make
```

### container

Alternatively, you can use the findent container: `gitlab-registry.mpcdf.mpg.de/tabriz/findent:latest`

It can be used for running findent in your CI environment. For example, in your GitLab CI yaml file (`.gitlab-ci.yml`) you can run the findent and also save the patch file as an artifact:

```yaml
style_check:
  image: gitlab-registry.mpcdf.mpg.de/tabriz/findent:latest
  script:
    - findent_batch --dir=./src --dry-run
  artifacts:
    paths:
        - findent.patch
    when: on_failure    
```

## usage

Findent reads from standard input and writes to standard output.

### findent

- display help

```bash
findent -h
```

- read the `in.f90` file, indent it and write to `out.f90`

```bash
findent < in.f90 > out.f90
```

- indent each block with 2 spaces

```bash
findent -i2 < in.f90 > out.f90
```

### findent_batch

`findent_batch` (located at `scripts/)` is a shell script for fixing or just checking all the fortran source files (\*.F90/\*.f90) in a given directory recursively.

By default, the script shows the changes proposed/made by the findent (git diff output) which can be suppressed by using the `--silent` switch. The path to the findent binary can also be modified from the command line.

>A patch file (`findent.patch`) is generated in dry-run mode which can be used to apply the style post-mortem:
>
>```shell
>git apply findent.patch
>```

- display help for the script

```bash
findent_batch --help
```

-  run findent on all the files in the `../src` directory recursively

```bash
findent_batch --dir=../src
```

- run findent but fix the files without showing the diffs

```bash
findent_batch --silent
```

- run findent but don't fix the files and generate a patch file (`findent.patch`) instead. Returns 0 if all the files were properly indented, otherwise returns 1

```bash
findent_batch --dry-run
```

- run findent but don't fix the files and generate the patch file at `/other/path`. Returns 0 if all the files were properly indented, otherwise returns 1

```bash
findent_batch --dry-run --patch-dir=/other/path
```

- use the findent binary at `~/findent/bin/findent` instead of the one in the path

```bash
findent_batch --findent-exec=~/findent/bin/findent
```

### vim integration

The following command shows instructions on how to use findent in (g)vim:

```bash
findent --vim_help
```

You can see the documentation in (g)vim by:

```vim
:help equalprg
:help indentexpr
```

or in `vim/README` and the comments in the files `vim/findent.vim` and `vim/fortran.vim`

### gedit integration

The following command shows instructions on how to use findent in gedit:

```bash
findent --gedit_help
```

### emacs integration

The following command shows instructions on how to use findent in emacs:

```bash
findent --emacs_help
```
